#/bin/sh

cd environment && docker build . -t essai; cd ..

# the following command should work on linux (same as last year)
docker run --rm --workdir /code --mac-address=00:00:00:00:00:00 --volume "$PWD/license":/MATLAB/licenses --volume "$PWD/data":/data --volume "$PWD/code":/code --volume "$PWD/result":/results essai /bin/bash -c "who; chmod +x run; ./run"