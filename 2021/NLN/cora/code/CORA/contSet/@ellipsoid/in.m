function res = in(obj1,obj2,mode)
% in - checks whether obj2 is contained in obj1
%
% Syntax:  
%    res = in(obj1,obj2)
%    res = in(obj,obj2,mode)
%
% Inputs:
%    obj1 - ellipsoid object 
%    obj2 - contSet object 
%    mode - mode of check ('exact' or 'approx')
%
% Outputs:
%    res - result of containment check
%
% Example: 
%    E1 = ellipsoid([5 7;7 13],[1;2]);
%    E2 = ellipsoid(0.3*eye(2));
%    zono = zonotope([0 1 0;0 1 1]);
%
%    in(E1,E2)
%    in(E1,zono)
%
%    figure
%    hold on
%    plot(E1,[1,2],'b');
%    plot(E2,[1,2],'g');
%
%    figure
%    hold on
%    plot(E1,[1,2],'b');
%    plot(zono,[1,2],'r');
%
% References:
%            [1] Yildirim, E.A., 2006. On the minimum volume covering 
%                ellipsoid of ellipsoids. SIAM Journal on Optimization, 
%                17(3), pp.621-641.     
%            [2] SDPT3: url: http://www.math.nus.edu.sg/~mattohkc/sdpt3.html
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann, Niklas Kochdumper
% Written:      15-October-2019 
% Last update:  21-November-2019 (NK, extend to other sets)
%               09-March-2021 (included tolerance for q comparison)
%               17-March-2021 (error handling)
% Last revision:---

%------------- BEGIN CODE --------------
if ~exist('mode','var')
    mode = 'exact';
end
if isa(obj2,'double')

    for i = 1:size(obj2,2)
        res = containsPoint(obj1,obj2(:,i)); 
        if ~res
            return;
        end
    end
elseif isa(obj2,'ellipsoid')
    res = inEllipsoid(obj1,obj2);
elseif isa(obj2,'zonotope') && strcmp(mode,'approx')
    obj2 = ellipsoid(obj2,'o:norm:bnd');
    res = inEllipsoid(obj1,obj2);
else
    if strcmp(mode,'exact')
        if ismethod(obj2,'vertices')
            % check if all vertices of the set are contained
            res = in(obj1,vertices(obj2));
        else
            error('Mode "exact" not implemented for second argument type!');
        end   
    else
        if ismethod(obj2,'zonotope')
            % zonotope over-approximation
            obj2 = zonotope(obj2); 
            res = in(obj1,obj2);
        else
            error('Not implemented for second argument type!');
        end
    end
end
%------------- END OF CODE --------------