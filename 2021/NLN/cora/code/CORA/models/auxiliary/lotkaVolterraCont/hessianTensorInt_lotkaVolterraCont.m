function Hf=hessianTensorInt_lotkaVolterraCont(x,u)



 Hf{1} = interval(sparse(6,6),sparse(6,6));

Hf{1}(1,1) = -2;
Hf{1}(2,1) = -17/20;
Hf{1}(5,1) = -1/2;
Hf{1}(1,2) = -17/20;
Hf{1}(1,5) = -1/2;


 Hf{2} = interval(sparse(6,6),sparse(6,6));

Hf{2}(2,1) = -1/2;
Hf{2}(1,2) = -1/2;
Hf{2}(2,2) = -2;
Hf{2}(3,2) = -17/20;
Hf{2}(2,3) = -17/20;


 Hf{3} = interval(sparse(6,6),sparse(6,6));

Hf{3}(3,2) = -1/2;
Hf{3}(2,3) = -1/2;
Hf{3}(3,3) = -2;
Hf{3}(4,3) = -17/20;
Hf{3}(3,4) = -17/20;


 Hf{4} = interval(sparse(6,6),sparse(6,6));

Hf{4}(4,3) = -1/2;
Hf{4}(3,4) = -1/2;
Hf{4}(4,4) = -2;
Hf{4}(5,4) = -17/20;
Hf{4}(4,5) = -17/20;


 Hf{5} = interval(sparse(6,6),sparse(6,6));

Hf{5}(5,1) = -17/20;
Hf{5}(5,4) = -1/2;
Hf{5}(1,5) = -17/20;
Hf{5}(4,5) = -1/2;
Hf{5}(5,5) = -2;
