\section{Benchmark Definitions} \label{sec:benchmarks}

\subsection{Input Parameterization}

\paragraph{Arbitrary piece-wise continuous input signals (Instance 1).}
This option leaves the input specification up to the participants.
The search space is, in principle, the entire set of piece-wise
continuous input signals (i.e., which permit discontinuities),
where the values for each individual
dimensions are from a given range.  Additional constraints that
were suggested are finite-number of discontinuity and finite
variability for all continuous parts of inputs.  Further, each
benchmark may impose further constraints.  Participants may
instruct their tools to search a subset of the entire search
space, notably to achieve finite parametrization, and then to
apply an interpolation scheme to synthesize the input signal.

However, the participants agreed that such a choice must be
``reasonable'' and should be justified from the problem's
specification without introducing external knowledge about
potential solutions.  Moreover, more general parametrizations that
are shared across requirements and benchmark models were
preferable.  Due to the diversity of benchmarks, it was decided to
evaluate the proposed solutions using common sense.

\paragraph{Constrained input signals (Instance 2).}  This option
precisely fixes the format of the input signal, potentially
allowing discontinuities. An example input signal would be
piecewise constant with $k$ equally spaced control points, with
ranges for each dimension of the input, disabling interpolation at
Simulink input ports so that tools don't need to up-sample their
inputs.  The arguments in favor of that are increased
comparability of results.  As possible downside was mentioned that
optimization-based tools (\STaLiRo and \Breach) are just compared with
respect to their optimization algorithm.
Nevertheless such a comparison is still meaningful, in particular,
as \FalStar and \falsify implement other approaches to falsification.

\subsection{Models and Requirements}

A brief description of the benchmark models follows,
the formal requirements are shown in Table~\ref{tab:requirements} as STL/MTL formulas.
The conjunctive requirements introduced in this year
are AT6abc, NNx, and CCx, as marked in the table.

\input{requirements.tex}

\paragraph{Automatic Transmission (AT).}

This model of an automatic transmission encompasses a controller
that selects a gear~1 to~4 depending on two inputs (throttle,
brake) and the current engine load, rotations per minute~$\omega$,
and car speed~$v$.  It is a standard falsification benchmark
derived from a model by Mathworks and has been proposed for
falsification in~\cite{ARCH14}.

Input specification: $0 \le \mathit{throttle} \le 100$ and
$0 \le \mathit{brake} \le 325$ (both can be active at the same
time).  Constrained input signals (instance~2) permit
discontinuities at most every 5~time units.
Requirements are specific versions of those in \cite{ARCH14} where the parameters have been chosen to be somewhat difficult. \\

% \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
% $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.

\paragraph{Fuel Control of an Automotive Powertrain (AFC).}

The model is described in \cite{JinHSCC2014} and has been used in
two previous instalments of this
competition~\cite{ARCH17falsification,ARCH18falsification}.  The
specific limits used in the requirements are chosen such that
falsification is possible but reasonably hard.

The constrained input signal (instance~2) fixes the
throttle~$\theta$ to be piecewise constant with 10 uniform
segments over a time horizon of 50 with two modes (normal and power
corresponding to feedback and feedforward control), and the engine
speed~$\omega$ to be constant with $900 \le \omega < 1100$ to
capture the input profile outlined in \cite{JinHSCC2014} and to
match the previous competitions.  For this reason, we do not
consider the unconstrained (instance~1) input specification.
Faults are disabled (e.g. by setting $\mathtt{fault\_time} > 50$).

\paragraph{Neural-network Controller (NN).}

This benchmark is based on MathWork's neural network controller for a system that levitates a magnet above an electromagnet at a reference position.%
\footnote{\url{https://au.mathworks.com/help/deeplearning/ug/design-narma-l2-neural-controller-in-simulink.html}}
It has been used previously as a falsification demonstration in the distribution of Breach.
The model has one input, a reference value $\mathit{Ref}$ for the position, where $1 \le \mathit{Ref}$ and $\mathit{Ref} \le 3$.
It outputs the current position of the levitating magnet~$\mathit{Pos}$.
The input specification for instance~1 requires discontinuities to be at least 3~time units apart,
whereas instance~2 specifies an input signal with exactly three constant segments.
The time horizon for the problem is~40.
The requirement ensures that after changes to the reference, the actual position eventually stabilizes around that value with small error.
% \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
% $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.

\paragraph{Wind Turbine (WT).}

The model is a simplified wind turbine model proposed
in~\cite{ARCH16:Hybrid_Modelling_of_Wind}.
The input\footnotemark{} of the
system is wind speed $v$ and the outputs are blade pitch angle
$\theta$, generator torque $M_{g, d}$, rotor speed $\Omega$ and
demanded blade pitch angle $\theta_d$.  The wind speed is
constrained by $8.0 \leq v \leq 16.0$.  Instance 1 allows any
piece-wise continuous inputs, while instance 2 constrains inputs
to piece-wise constant signals whose control points which are
evenly spaced each 5 seconds.  The model is relatively large.
Further, the time horizon is long (630) compared to other benchmarks.

\footnotetext{Organizer's comment:
The status of the wind turbine benchmark is currently unresolved.
The model contains an alternative setup,
where the wind speed is not a time-varying input provided by the falsification tool,
but taken from one of 13 preset inputs. It remains unclear how
this mechanism interacts with the input port for~$v$ of the Simulink model.
Until this issue is resolved, the model is exempt from validation.}

% \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
% $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.

\paragraph{Chasing cars (CC).}

The model is derived from Hu et al.~\cite{hu2000towards} which
presents a simple model of an automatic chasing car.  Chasing cars
(CC) model consists of five cars, in which the first car is driven
by inputs ($\mathit{throttle}$ and $\mathit{brake}$), and other
four are driven by Hu et al.'s algorithm.  The output of the
system is the location of five cars $y_1, y_2, y_3, y_4, y_5$.
The properties to be falsified are constructed artificially, to
investigate the impact of complexity of the formulas to
falsification.  The input specifications for instance 1 allows any
piecewise continuous signals while the input specification for
instance 2 constraints inputs to piecewise constant signals with
control points for each 5 seconds, i.e., 20 segments.

%   \falsify: Input specification for Instance 1 uses piecewise constant function with discontinuities spaced in even intervals $\Delta T$.
%   $\Delta T = 1$ for all models except for SC in which $\Delta T = 0.1$ is used.


\paragraph{Aircraft Ground Collision Avoidance System (F16).}
The model has been derived
from the one presented in~\cite{ARCH18:Verification_Challenges_in_F_16}.
The F16
aircraft and its inner-loop controller for Ground Collision
avoidance have been modeled using 16 continuous variables with
piece-wise nonlinear differential equations. Autonomous maneuvers
are performed in an outer-loop controller that uses a finite-state
machine with guards involving the continuous variables. The system
is required to always avoid hitting the ground during its maneuver
starting from all the initial conditions for roll, pitch, and yaw
in the range
$[0.2\pi, 0.2833\pi] \times [-0.4\pi,-0.35\pi] \times [-0.375\pi,-0.125\pi]$.%
    \footnote{The report from 2019~\cite{ernst2019arch} erroneously specifies:
$[0.2\pi, 0.2833\pi] \times [-0.5\pi,-0.54\pi] \times [0.25\pi, 0.375\pi]$,
              however, previous results were in fact obtained with the correct range.
    }
Since the benchmark has no time-varying input, there
is no distinction between instance~1 and instance~2.  The
requirement is checked for a time horizon equal to 15.

\paragraph{Steam condenser with Recurrent Neural Network Controller (SC).}

The model is presented in \cite{YaghoubiHSCC}. It is a dynamic model of an steam condenser based on energy balance and cooling water mass balance controlled with a Recurrent Neural network in feedback. The time horizon for the problem is 35 seconds. The input to the system can vary in the range $[3.99, 4.01]$.
For instance~2, the input signal should be piecewise constant with 20~evenly spaced segments.

% \falsify: We choose $\Delta T = 0.1$ for SC model because Instance 2 uses $\Delta T = 1.75$, which is near to $\Delta T = 1$.


