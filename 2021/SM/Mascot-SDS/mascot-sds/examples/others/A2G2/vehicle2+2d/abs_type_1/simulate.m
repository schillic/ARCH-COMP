%
% simulate.m
%
% created on: 09.10.2015
%     author: rungger (vehicle.m)
%     modified by: kaushik
%
%
% you need to run ./vehicle binary first 
%
% so that the file: vehicle_controller_under.bdd is created
%

function simulate
    clear set
    close all
    addpath(genpath('../../../../mfiles'))

    %% simulation
    openfig('vehicle_domain')
    hold on
    % initial state
    x0=[0.4,0.2,0.0,1.0];
    eps = 0.00001;
    
    % door in [x,y,w,h] format
%     door = [1.1, 0.9, 0.1, 0.8];
    door = [0, 0.8, 1.1, 0.1];
    dHandle = rectangle('Position', door, 'EdgeColor', 'k', 'LineWidth', 3, 'LineStyle', '--');
    if (x0(3)==0.0)
        dHandle.FaceColor = [0.7,0.7,0.7];
    end
    % office (place of request) in [x,y,w,h] format
    office = [0.3, 1.3, 0.4, 0.4];
    oHandle = rectangle('Position', [0.3, 1.3, 0.4, 0.4], 'EdgeColor', 'g', 'LineWidth', 3);
%     oHandle = viscircles([0.5, 1.5], 0.05, 'Color', 'r');
    if (x0(4)==0.0)
        oHandle.Visible = 'off';
    end
    % kitchen in [x,y,w,h] format
    kitchen = [1.3, 0.3, 0.4, 0.4];
    
%     % real obstacle rectangle in the format [x y w h]
%     avoid = [0.8, 1.0, 0.4, 0.4];
% 
%     % real target rectangle in the format [x y w h]
%     target = [0.8, 0.2, 0.4, 0.4];
% 
    % state space bounds in the form [xmin xmax ymin ymax]
    xlim = [0 2 0 2];

    % parameters for the simulation
    [~,~,W_ub,~,~,tau] = readParams('input.hh');

    % prepare the figure window
%     figure
    axis(xlim);

    % load the symbolic set containing the controller
    C0=SymbolicSet('vehicle_controller_under_0.bdd');
    C1=SymbolicSet('vehicle_controller_under_1.bdd');

%     % plot the initial state, target, and obstacles
%     plot(x0(1),x0(2),'k*');
%     rectangle('Position',target,'FaceColor',[51, 204, 51]/255);
%     rectangle('Position',avoid,'FaceColor','k');
%     % label the initial state, target, and obstacles
%     text(0.94,2.44,'$I$','interpreter','latex','FontSize',16);
%     text(0.74,0.94,'$A$','interpreter','latex','FontSize',16);
%     text(0.75,0.12,'$B$','interpreter','latex','FontSize',16);
    
    % simulate T time-steps
    y=x0;
    v=[];
    T=1000; 
    CurCont=0; % index of the current controller
    for t=1:T
        disp(t)
        if (CurCont==0)
            u=C0.getInputs(y(end,:));
        else
            u=C1.getInputs(y(end,:));
        end
        % switch the controller upon reaching the current goal
        if (CurCont==0 ...
            && atOffice(y(end,1:2))...
            && y(end,4)==1)
            CurCont = 1;
        elseif (CurCont==1 ...
                 && atKitchen(y(end,1:2)))
            CurCont = 0;
        end

        v=[v; u(1,:)];
        x = vehicle(y(end,:),v(end,:)); 

        y=[y; x(end,:)];
        plot(y(end-1:end,1),y(end-1:end,2),'r.-');
        h1 = plot(y(end,1),y(end,2),'ko','MarkerSize',10)

        pause(0.1)
        delete(h1);
        
    end

    savefig('traj_stochastic');
    
    function status = atDoor(x)
        if (x(1)-door(1)>eps...
            && x(2)-door(2)>eps...
            && x(1)-(door(1)+door(3))<eps...
            && x(2)-(door(2)+door(4))<eps)
            status = true;
        else
            status = false;
        end
    end
    
    function status = atOffice(x)
        if (x(1)-office(1)>eps...
            && x(2)-office(2)>eps...
            && x(1)-(office(1)+office(3))<eps...
            && x(2)-(office(2)+office(4))<eps)
            status = true;
        else
            status = false;
        end
    end

    function status = atKitchen(x)
        if (x(1)-kitchen(1)>eps...
            && x(2)-kitchen(2)>eps...
            && x(1)-(kitchen(1)+kitchen(3))<eps...
            && x(2)-(kitchen(2)+kitchen(4))<eps)
            status = true;
        else
            status = false;
        end
    end

    function xn = vehicle(x,u)
        % uniform random noise
        w = 2*W_ub(1:2).*(rand(1,2))' - W_ub(1:2);
        % worst case noise
    %         w = wmax;
        xn = zeros(size(x));
        xn(1) = x(1) + u(1)*tau + w(1);
        xn(2) = x(2) + u(2)*tau + w(2);
        
        if (x(3)==0.0) % if the door is closed, it may open in the next time step with
        % some probability
            r = rand;
            if (r<0.03)
                xn(3) = 1.0;
                dHandle.FaceColor = 'none';
            else
                xn(3) = 0.0;
                dHandle.FaceColor = [0.7,0.7,0.7];
            end
        else % when the door is opened, it remains open until the robot reaches the kitchen
            if (atKitchen(x))
                xn(3) = 0.0;
                dHandle.FaceColor = [0.7,0.7,0.7];
            else
                xn(3) = 1.0;
                dHandle.FaceColor = 'none';
            end
        end
        
        if (x(4)==0.0) % if there is no request, a request appears with some probability
            r = rand;
            if (r<0.05)
                xn(4) = 1.0;
                oHandle.Visible = 'on';
            else
                xn(4) = 0.0;
                oHandle.Visible = 'off';
            end
        else % when a request appears, it remains active until it gets served
            if (atOffice(x))
                xn(4) = 0.0;
                oHandle.Visible = 'off';
            else
                xn(4) = 1.0;
                oHandle.Visible = 'on';
            end
        end
    end

end



