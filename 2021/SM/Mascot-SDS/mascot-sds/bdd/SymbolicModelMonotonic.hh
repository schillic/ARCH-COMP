/*
 * SymbolicModelMonotonic.hh
 *
 *  created on: 09.10.2015
 *      author: rungger (original file=SymbolicModelGrowthBound.hh)
 *      modified by: kaushik mallik
 */

#ifndef SYMBOLICMODELMONOTONIC_HH
#define SYMBOLICMODELMONOTONIC_HH

#include <iostream>
#include <stdexcept>


#include "TicToc.hh"

#include "cuddObj.hh"
#include "cudd.h"
#include "dddmp.h"


#include "SymbolicSet.hh"
#include "SymbolicModel.hh"

#define CHUNK_SIZE 800

namespace scots {
/*
 * class: SymbolicModelMonotonic
 *
 * Derived class from <SymbolicModel>
 *
 * Constructs a symbolic model according to
 * the theory in http://arxiv.org/abs/1503.03715
 *
 */

template<class stateType_, class inputType_>
class SymbolicModelMonotonic: public SymbolicModel {
public:
  /* constructor: see <SymbolicModel>
   */
  using SymbolicModel::SymbolicModel;
  /* function:  computeTransitions
   *
   * Approximate the transitions using mixed-monotonicity property
   * Assumption: a decomposition function is available
   *          which is a function of the input and two points in the state space
   *
   * We do not consider measurement errors
   *
   */
  template<class F1>
  void computeTransitions(F1 &decomposition, const stateType_ &W_ub) {

    /* create the BDD's with numbers 0,1,2,.., #gridPoints */
    size_t dim=stateSpace_->getDimension();
    const size_t* nvars= stateSpace_->getNofBddVars();
    BDD **bddVars = new BDD*[dim];
    for(size_t n=0, i=0; i<dim; i++) {
      bddVars[i]= new BDD[nvars[i]];
      for(size_t j=0; j<nvars[i]; j++)  {
        bddVars[i][j]=ddmgr_->bddVar(postVars_[n+j]);
      }
      n+=nvars[i];
    }
    const size_t* ngp= stateSpace_->getNofGridPoints();
    BDD **num = new BDD*[dim];
    for(size_t i=0; i<dim; i++) {
      num[i] = new BDD[ngp[i]];
      int *phase = new int[nvars[i]];
      for(size_t j=0;j<nvars[i];j++)
        phase[j]=0;
      for(size_t j=0;j<ngp[i];j++) {
        int *p=phase;
        int x=j;
        for (; x; x/=2) *(p++)=0+x%2;
        num[i][j]= ddmgr_->bddComputeCube(bddVars[i],(int*)phase,nvars[i]);
      }
      delete[] phase;
      delete[] bddVars[i];
    }
    delete[] bddVars;

    /* bdd nodes in pre and input variables */
    DdManager *mgr = ddmgr_->getManager();
    size_t ndom=nssVars_+nisVars_;
    int*  phase = new int[ndom];
    DdNode**  dvars = new DdNode*[ndom];
    for(size_t i=0;i<nssVars_; i++)
      dvars[i]=Cudd_bddIthVar(mgr,preVars_[i]);
    for(size_t i=0;i<nisVars_; i++)
      dvars[nssVars_+i]=Cudd_bddIthVar(mgr,inpVars_[i]);
    /* initialize cell radius
     * used to compute the corners of the cell */
    stateType_ eta, r;
    stateSpace_->copyEta(&eta[0]);
    /* the first grid points are used to compute the id of the current grid point */
    stateType_ first;
    stateSpace_->copyFirstGridPoint(&first[0]);
    /* initialize transitions as empty set */
    maybeTransition_=ddmgr_->bddZero();
    sureTransition_ =ddmgr_->bddZero();
    /* compute constraint set against the post is checked */
    size_t n=ddmgr_->ReadSize();
    int* permute = new int[n];
    for(size_t i=0; i<nssVars_; i++)
      permute[preVars_[i]]=postVars_[i];
    BDD ss = stateSpace_->getSymbolicSet();
    BDD constraints=ss.Permute(permute);
    delete[] permute;

      for(begin(); !done(); next()) {
        progress();
        const int* minterm=currentMinterm();
        addTransitions(minterm, decomposition, r, dim, eta, first, W_ub,
                        constraints, ngp, num, phase, mgr, dvars, ndom);
      }



    for(size_t i=0; i<dim; i++)
      delete[] num[i];
    delete[] num;
    delete[] dvars;
    delete[] phase;
  }

private:
  template<class F1>
  void addTransitions(const int* minterm, F1 &decomposition,
                      stateType_ &r, size_t dim,
                      stateType_ eta, stateType_ first,
                      const stateType_ &W_ub,
                      BDD constraints, const size_t* ngp, BDD** num, int* phase,
                      DdManager* mgr, DdNode** dvars, size_t ndom) {
    /* current state */
    stateType_ x;
    stateSpace_->mintermToElement(minterm,&x[0]);
    /* current input */
    inputType_ u;
    inputSpace_->mintermToElement(minterm,&u[0]);
    /* cell radius (including measurement errors) */
    for(size_t i=0; i<dim; i++)
      r[i]=eta[i]/2.0;

    /* Compute the corners of the current state */
    stateType_ x_lb, temp;
    stateType_ x_ub;
    for (size_t i = 0; i < dim; i++) {
      x_lb[i] = x[i]-r[i];
      x_ub[i] = x[i]+r[i];
    }
    /* compute the corners of the nominal recahble set */
    temp = decomposition(u,x_lb,x_ub); /* temporary storage for x_lb */
    x_ub = decomposition(u,x_ub,x_lb);
    x_lb  = temp;

    /************ Compute the maybe transitions ******************/
    /* compute the over-approximation of the perturbed reachable set */
    stateType_ outer_lb, outer_ub;
    int lb, ub;
    for (size_t i = 0; i < dim; i++) {
      outer_lb[i] = x_lb[i]-W_ub[i];
      outer_ub[i] = x_ub[i]+W_ub[i];
    }
    /* determine the cells which intersect with the overapproximation set*/
    BDD post=ddmgr_->bddOne();
    for(size_t i=0; i<dim; i++) {
      lb = std::lround(((outer_lb[i]-first[i])/eta[i]));
      ub = std::lround(((outer_ub[i]-first[i])/eta[i]));
      if(lb<0 || ub>=(int)ngp[i]) {
        post=ddmgr_->bddZero();
        break;
      }
      BDD zz=ddmgr_->bddZero();
      for(int j=lb; j<=ub; j++) {
        zz|=num[i][j];
      }
      post &= zz;
    }

    bool compute_sure_flag;
    if(!(post==ddmgr_->bddZero()) && post<= constraints) {
      /* compute bdd for the current x and u element and add x' */
      for(size_t i=0;i<nssVars_; i++)
        phase[i]=minterm[preVars_[i]];
      for(size_t i=0;i<nisVars_; i++)
        phase[nssVars_+i]=minterm[inpVars_[i]];
      BDD current(*ddmgr_,Cudd_bddComputeCube(mgr,dvars,phase,ndom));
      current&=post;
      maybeTransition_ +=current;
      compute_sure_flag = true;
    } else { /* both the maybe and the sure transitions are empty */
        /* skip compuation of sure transitions */
        compute_sure_flag = false;
    }

    if (compute_sure_flag) {
        /********** Compute the sure transitions *****************/
          /* compute the under-approximation of the perturbed reachable set */
          for (size_t i = 0; i < dim; i++) {
              /* the reachable set is obtained by taking offset from the boundary */
              outer_lb[i] = x_ub[i]-W_ub[i];
              outer_ub[i] = x_lb[i]+W_ub[i];
          }
          /* determine the cells which intersect with the reachable set*/
          BDD post_sure=ddmgr_->bddOne();
          for(size_t i=0; i<dim; i++) {
            lb = std::lround(((outer_lb[i]-first[i])/eta[i]));
            ub = std::lround(((outer_ub[i]-first[i])/eta[i]));
            if(lb<0 || ub>=(int)ngp[i]
               || ((outer_lb[i]>=outer_ub[i]) && (lb!=ub))) {
              post_sure=ddmgr_->bddZero();
              break;
            }
            BDD zz=ddmgr_->bddZero();
            for(int j=lb; j<=ub; j++) {
              zz|=num[i][j];
            }
            post_sure &= zz;
          }
        /* check the constraint satisfaction and non-emptiness of transition */
        if(!(post_sure==ddmgr_->bddZero()) && post_sure<= constraints) {
            /* compute bdd for the current x and u element and add x' */
            for(size_t i=0;i<nssVars_; i++)
              phase[i]=minterm[preVars_[i]];
            for(size_t i=0;i<nisVars_; i++)
              phase[nssVars_+i]=minterm[inpVars_[i]];
            BDD current(*ddmgr_,Cudd_bddComputeCube(mgr,dvars,phase,ndom));
            current&=post_sure;
            sureTransition_ +=current;
        }
    }

  }
}; /* close class def */


} /* close namespace */

#endif /* SYMBOLICMODELGROWTHBOUND_HH_ */
