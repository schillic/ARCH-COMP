
%NOTE: Run THIS Script for excel result files
   
%FIRST:Import the Entire Log File AS A CELL ARRAY

%SECOND: Change the right side of the line below to the name of the newly created import variable

%%%%% Change the RHS As Apropriate Below %%%%%
results = FML1C2; %adjust rhs to match import
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nT_SOAR=[];
nR_SOAR=[];
numOfFals_SOAR=0;
outer_runs_total = size(results,1);

for i = 1:outer_runs_total
    if results{i,6} <= 0
        nT_SOAR=[nT_SOAR, results{i,4}];
        numOfFals_SOAR=numOfFals_SOAR+1;
    else
        nR_SOAR=[nR_SOAR; results{i,6}];
    end
end

disp(['number of falsifications: ',num2str(numOfFals_SOAR),'/50'])
disp(['Average number of runs to falsify: ', num2str(mean(nT_SOAR))])
disp(['Median number of runs to falsify: ', num2str(median(nT_SOAR))])