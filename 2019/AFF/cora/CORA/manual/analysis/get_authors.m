function [authors, no_authors] = get_authors(directory)
% get_authors - returns the name of the authors for each m. file in the 
% CORA directory
%
% Syntax:  
%    get_authors(directory)
%
% Inputs:
%    directory: a string contains the full directory of the CORA files.
%
% Outputs:
%    authors: a struct with three different fields:
%         1. author_name: a string contains the name of the author.
%         2. file_name: name of the file for which the author is found.
%         3. file_path: full path of the file for which the author is
%            found.
%     
%     no_authors: a struct containing the files with no authors. It has two 
%     fields:
%         1. file_name: the name of the file with no author.
%         2. file_path: the path of the file with no author.
% 
%
% 
% Author:       Raja Judeh
% Written:      February-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%% Getting all the .m files in the given directory 

m_files = dir([directory, '/**/*.m']);

%% Defining some variables

num_files = length(m_files); %number of m. files
N_author = 0; %number of files with authors
N_noAuthor = 0; %number of files with NO authors

%% Extract authors for all m. files. Keep track of files with NO authors.

for f = 1:num_files 
    file_name = m_files(f).name; %the name of the current m. file
    file_path = m_files(f).folder; %the path of the current m. file
    file = fileread([file_path,'\',file_name]); %read the current .m file

    % Look for the line that contains the keyword 'Author'
    line = regexp(file, '[^\n]*Author*[^\n]*', 'match'); %returns a cell
    
    % Check if there is an author in the current m. file
    if isempty(line) %if NO author exists
        N_noAuthor = N_noAuthor + 1; %increase number of NO authors by 1 
        
        % A struct containing all the files that have NO authors
        no_authors(N_noAuthor).file_name = file_name; %add the file_name
        no_authors(N_noAuthor).file_path = file_path; %add the file_path
    else %if author exists
        N_author = N_author + 1; %increase number of authors by 1
        author = extractAfter(cell2mat(line), "Author: "); %author's name   
        author = strtrim(author(1:end-1)); %discard the last character

        % A struct containing all the files that have authors
        authors(N_author).author_name = author; %add the author_name
        authors(N_author).file_name = file_name; %add the file_name
        authors(N_author).file_path = file_path; %add the file_path
    end
end

%------------- END OF CODE --------------

end