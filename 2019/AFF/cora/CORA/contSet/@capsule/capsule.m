classdef capsule
% capsule - Object and Copy Constructor 
%
% Syntax:  
%    object constructor: Obj = capsule(varargin)
%    copy constructor: Obj = otherObj
%
% Inputs:
%    input1 - center
%    input2 - generator
%    input3 - radius
%
% Outputs:
%    Obj - Generated Object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: intervalhull,  polytope

% Author:       Matthias Althoff
% Written:      04-March-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = protected, GetAccess = public)
    c = []; % center
    g = []; % generator
    r = 0; % radius
    contSet = [];
end
   
methods

    function Obj = capsule(varargin)

        % If 1 argument is passed
        if nargin == 1
            %is input a zonotope?
            if isa(varargin{1},'capsule')
                Obj = varargin{1};
            else
                if isvector(varargin{1})
                    % set center
                    Obj.c = varargin{1}; 
                elseif isscalar(varargin{1})
                    % set radius
                    Obj.r = varargin{1}; 
                end
            end
        elseif nargin == 2
            if isvector(varargin{1}) && isvector(varargin{2})
                % set center and generator
                Obj.c = varargin{1}; 
                Obj.g = varargin{2};
            elseif isvector(varargin{1}) && isscalar(varargin{2})
                % set center and radius
                Obj.c = varargin{1}; 
                Obj.r = varargin{2};
            end
        else
            % set all values
            Obj.c = varargin{1}; 
            Obj.g = varargin{2};
            Obj.r = varargin{3};
        end
        
        %Generate parent object
        if ~isempty(varargin{1}) && ~isa(varargin{1}, 'capsule') && isvector(varargin{1})
            Obj.contSet = contSet(length(varargin{1}(:,1)));
        else
            %cSet=contSet(0);
            Obj.contSet = contSet(0);
        end
    end
end
end
%------------- END OF CODE --------------