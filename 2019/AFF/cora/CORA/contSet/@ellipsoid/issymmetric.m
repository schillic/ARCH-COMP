function res = issymmetric(varargin)
% issymmetric - Checks whether matrix is symmetric
%
% Syntax:  
%    res = issymmetric(E) Computes whether E.Q is symmetric using E.TOL
%    res = issymmetric(Q) Computes whether Q is symmetric using TOL=1e-11
%    res = issymmetric(Q,TOL) Computes whether Q is symmetric using TOL
% Inputs:
%    E - ellipsoids object
%    Q - matrix to check
%    TOL - used tolerance
%
% Outputs:
%    res - logical indicating whether E.Q is symmetric
%
% Example: 
%    E = ellipsoid([1,0;0,1/2],[1;1]);
%    res = issymmetric(E);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      27-July-2016
% Last update:  ---
% Last revision:---
%------------- BEGIN CODE --------------
TOL = 1e-11;
Q = [];
switch nargin
    case 1
        if isa(varargin{1},'ellipsoid')
            Q = varargin{1}.Q;
            TOL = varargin{1}.TOL;
        else
            Q = varargin{1};
        end
    case 2
        Q = varargin{1};
        TOL = varargin{2};
    otherwise
        error('Not enough/too many input arguments');
end
        
diff_mat=abs(triu(Q)-tril(Q)');
res = ~any(any(diff_mat>TOL,1));
%------------- END OF CODE --------------