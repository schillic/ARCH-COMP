# ARCH2019_RE

This is the repeatability evaluation package for the 3rd International
Competition on Verifying Continuous and Hybrid Systems Friendly Competition
(ARCH-COMP '19).

This year the ARCH workshop will be held on April 15, 2019, in Montreal, as part
of [Cyber-Physical Systems and Internet-Of-Things
Week](http://cpslab.cs.mcgill.ca/cpsiotweek2019/).

## Installation

To build the container, you need the program `docker`.
For installation instructions on different platforms, consult
[the Docker documentation](https://docs.docker.com/install/).
For general information about `Docker`, see
[this guide](https://docs.docker.com/get-started/).

Once you have installed Docker, first clone this repository. You can
clone only the SpaceEx files as follows:

```shell
mkdir ARCH-COMP-2019
cd ARCH-COMP-2019
git init
git remote add -f origin https://gitlab.com/goranf/ARCH-COMP.git
git config core.sparseCheckout true
echo "2019/AFF/SpaceEx/" >> .git/info/sparse-checkout
git pull origin master
cd 2019/AFF/SpaceEx
```

Then build the docker image for SpaceEx:

```shell
docker build . -t spaceex
```

## Running the benchmarks

To run the container on the benchmarks, use the provided shell script:

```shell
./docker_SpaceEx_AFF.sh
```

To run SpaceEx with your own arguments, use:

```shell
docker run spaceex <your-arguments-here>
```

## Plots

To use the container to produce the plots, use the provided shell script:

```shell
./docker_SpaceEx_AFF_plots.sh
```

