from src.utils.formula_visitors.immutable_formula_visitor_interface import FormulaVisitorI
from src.utils.utils import get_widest_bounds
from src.verification.bounds.bounds import HyperRectangleBounds


class FormulaMILPBuilderVisitor(FormulaVisitorI):
    def __init__(self, constrs_manager, state_vars, agent, env):
        """
        An immutable visitor implementation for constructing a single MILP from a formula.
        :param constrs_manager: Constraints manager.
        :param state_vars: The current state of the env.
        :param agent: The neural agent.
        :param env: The non-deterministic environment.
        :side-effects: Modifies constrs_manager and state_vars.
        """
        self.state_vars = state_vars
        self.constrs_manager = constrs_manager
        self.bf = env.get_branching_factor()
        self.agent = agent
        self.env = env
        self.next_vars = self.state_vars

    def visitConstraintFormula(self, element):
        constrs_to_add = [self.constrs_manager.get_atomic_constraint(element, self.state_vars)]
        return constrs_to_add

    def visitVarVarConstraintFormula(self, element):
        return self.visitConstraintFormula(element)

    def visitVarConstConstraintFormula(self, element):
        return self.visitConstraintFormula(element)

    def visitNAryDisjFormula(self, element):

        split_vars = self.constrs_manager.create_binary_variables(len(element.clauses))
        self.constrs_manager.update()

        constrs = []
        for i in range(len(element.clauses)):
            current_clause_constrs = element.clauses[i].acceptI(self)

            for disj_constr in current_clause_constrs:
                if disj_constr._sense != 'I':  # Hack to check if indicator constraint.
                    constrs.append(self.constrs_manager.create_indicator_constraint(split_vars[i], 1, disj_constr))
                else:
                    constrs.append(disj_constr)

        # exactly one variable must be true
        constrs.append(self.constrs_manager.get_sum_constraint(split_vars, 1))

        return constrs

    def visitDisjFormula(self, element):
        [d] = self.constrs_manager.create_binary_variables(1)
        self.constrs_manager.update()

        constrs_to_add = []

        left_x1_constrs = element.left.acceptI(self)
        for constr in left_x1_constrs:
            if constr._sense != 'I':  # Hack to check if indicator constraint.
                constrs_to_add.append(self.constrs_manager.create_indicator_constraint(d, 1, constr))
            else:
                constrs_to_add.append(constr)

        right_x2_constrs = element.right.acceptI(self)
        for constr in right_x2_constrs:
            if constr._sense != 'I':  # Check if indicator constraint.
                constrs_to_add.append(self.constrs_manager.create_indicator_constraint(d, 0, constr))
            else:
                constrs_to_add.append(constr)

        return constrs_to_add

    def visitAtomicDisjFormula(self, element):
        return self.visitDisjFormula(element)

    def visitAtomicConjFormula(self, element):
        return self.visitConjFormula(element)

    def visitConjFormula(self, element):
        left_constraints = element.left.acceptI(self)
        right_constraints = element.right.acceptI(self)
        constrs_to_add = left_constraints + right_constraints
        return constrs_to_add

    def visitENextFormula(self, element):
        k = element.k
        if k == 1:
            smaller_formula = element.left
        else:
            from src.utils.formula import ENextFormula
            smaller_formula = ENextFormula(k - 1, element.left)

        constrs_to_add = []

        root_state_vars = self.state_vars

        # Add constraints for agent network.
        action_grb_vars, action_constrs = self.agent.get_constraints_for_action(self.constrs_manager, root_state_vars)
        constrs_to_add.extend(action_constrs)
        self.constrs_manager.get_variable_tracker().add_action_variables(action_grb_vars)

        d = self.constrs_manager.create_binary_variables(self.bf)
        constrs_to_add.append(self.constrs_manager.get_sum_constraint(d, 1))

        next_state_vars = self.constrs_manager.create_state_variables(len(self.state_vars))
        output_bounds = [(float("inf"), float("-inf")) for _ in range(len(next_state_vars))]  # Widest upper and lower bounds for output vars.
        self.constrs_manager.get_variable_tracker().add_state_variables(next_state_vars)

        self.constrs_manager.update()
        for i in range(self.bf):

            # Add and get constraints for transition function.
            output_state_vars, output_state_constrs = \
                self.env.get_constraints_for_transition(i, self.constrs_manager, action_grb_vars, root_state_vars)

            # Compute max possible upper and min possible lower bounds for each output var.
            get_widest_bounds(output_bounds, output_state_vars)

            next_var_constrs = [nsv == osv for nsv, osv in zip(next_state_vars, output_state_vars)]

            for constr in (output_state_constrs + next_var_constrs):
                if constr._sense != 'I':  # Check if indicator constraint.
                    constrs_to_add.append(self.constrs_manager.create_indicator_constraint(d[i], 1, constr))
                else:
                    constrs_to_add.append(constr)

        output_lower, output_upper = zip(*output_bounds)  # Unzip the bounds.
        self.constrs_manager.add_variable_bounds(next_state_vars, HyperRectangleBounds(output_lower, output_upper))

        self.state_vars = next_state_vars
        left_constraints = smaller_formula.acceptI(self)
        constrs_to_add.extend(left_constraints)

        return constrs_to_add

    def visitANextFormula(self, element):
        k = element.k
        if k == 1:
            smaller_formula = element.left
        else:
            from src.utils.formula import ANextFormula
            smaller_formula = ANextFormula(k - 1, element.left)

        constrs_to_add = []
        root_state_vars = self.state_vars

        for i in range(self.bf):
            # Add constraints for agent network.
            action_grb_vars, action_constrs = self.agent.get_constraints_for_action(self.constrs_manager, root_state_vars)
            constrs_to_add.extend(action_constrs)

            # Add and get constraints for transition function.
            output_state_vars, output_state_constrs = \
                self.env.get_constraints_for_transition(i, self.constrs_manager, action_grb_vars, root_state_vars)

            constrs_to_add.extend(output_state_constrs)
            self.state_vars = output_state_vars
            constrs_to_add.extend(smaller_formula.acceptI(self))

        return constrs_to_add

