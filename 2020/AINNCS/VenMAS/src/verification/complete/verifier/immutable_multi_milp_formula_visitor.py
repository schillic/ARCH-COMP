import functools
from src.utils.formula_visitors.immutable_formula_visitor_interface import FormulaVisitorI
from gurobipy import *

from src.verification.bounds.bounds import HyperRectangleBounds
from src.verification.complete.verifier.immutable_milp_visitor import get_widest_bounds


class MILP:
    def __init__(self, constrs_to_add):
        self.constrs_to_add = constrs_to_add


class FormulaMultiMILPBuilderVisitor(FormulaVisitorI):
    # TODO: Replace all lists with sets.
    def __init__(self, constrs_manager, state_vars, agent, env):
        """
        An immutable visitor implementation for constructing a set of MILPs from a formula.
        :param constrs_manager: Constraints manager.
        :param state_vars: The current state of the env.
        :param bf: The branching factor of the transition function
        :param agent: The neural agent.
        :param env: The non-deterministic environment.
        :side-effects: Modifies constrs_manager and state_vars.
        """
        self.state_vars = state_vars
        self.constrs_manager = constrs_manager
        self.bf = env.get_branching_factor()
        self.agent = agent
        self.env = env
        self.next_vars = self.state_vars
        self.constrs_to_add = []

    def visitConstraintFormula(self, element):
        self.constrs_to_add = [self.constrs_manager.get_atomic_constraint(element, self.state_vars)]
        return [self.constrs_to_add]

    def visitVarVarConstraintFormula(self, element):
        constrs = self.visitConstraintFormula(element)
        return constrs

    def visitVarConstConstraintFormula(self, element):
        constrs = self.visitConstraintFormula(element)
        return constrs

    def visitDisjFormula(self, element):
        left_constrs = element.left.acceptI(self)
        right_constrs = element.right.acceptI(self)
        # left_constrs and right_constrs already have the same root variables
        constrs = left_constrs + right_constrs
        return constrs

    def visitAtomicDisjFormula(self, element):
        left_constr = element.left.get_custom_atomic_constraint(self.state_vars)
        right_constr = element.right.get_custom_atomic_constraint(self.state_vars)
        constrs = []
        # left_constrs and right_constrs already have the same root variables
        deltas = self.constrs_manager.create_binary_variables(2)
        constrs.append(self.constrs_manager.create_indicator_constraint(deltas[0], 1, left_constr))
        constrs.append(self.constrs_manager.create_indicator_constraint(deltas[1], 1, right_constr))
        constrs.append(self.constrs_manager.get_sum_constraint(deltas, 1))
        return [constrs]

    def visitAtomicConjFormula(self, element):
        left_constr = element.left.get_custom_atomic_constraint(self.state_vars)
        right_constr = element.right.get_custom_atomic_constraint(self.state_vars)
        # left_constrs and right_constrs already have the same root variables
        constrs = [left_constr, right_constr]
        return [constrs]

    def visitConjFormula(self, element):
        left_constrs = element.left.acceptI(self)
        right_constrs = element.right.acceptI(self)
        # left_constrs and right_constrs already have the same root variables
        constrs = [l + r for l, r in itertools.product(left_constrs, right_constrs)]
        return constrs

    def visitENextFormula(self, element):

        # Add constraints for agent network.
        action_vars, action_constrs = self.agent.get_constraints_for_action(self.constrs_manager, self.state_vars)
        self.constrs_manager.get_variable_tracker().add_action_variables(action_vars)

        next_state_vars = self.constrs_manager.create_state_variables(len(self.state_vars))
        output_bounds = [(float("inf"), float("-inf")) for _ in range(len(next_state_vars))]  # Widest upper and lower bounds for output vars.
        self.constrs_manager.get_variable_tracker().add_state_variables(next_state_vars)

        constrs = []
        for i in range(self.bf):
            constrs_to_add = []

            # Add and get constraints for transition function.
            output_state_vars, output_state_constrs = \
                self.env.get_constraints_for_transition(i, self.constrs_manager, action_vars, self.state_vars)
            constrs_to_add.extend(output_state_constrs)

            # Compute max possible upper and min possible lower bounds for each output var.
            get_widest_bounds(output_bounds, output_state_vars)

            constrs_to_add.extend([self.constrs_manager.get_equality_constraint(nsv, osv)
                                   for nsv, osv in zip(next_state_vars, output_state_vars)])

            constrs.append(constrs_to_add)

        output_lower, output_upper = zip(*output_bounds)  # Unzip the bounds.
        self.constrs_manager.add_variable_bounds(next_state_vars, HyperRectangleBounds(output_lower, output_upper))

        k = element.k
        if k == 1:
            smaller_formula = element.left
        else:
            from src.utils.formula import ENextFormula
            smaller_formula = ENextFormula(k - 1, element.left)
        self.state_vars = next_state_vars
        left_constraints = smaller_formula.acceptI(self)

        product = [a + l + r for a, l, r in itertools.product([action_constrs], constrs, left_constraints)]
        return product

    def visitANextFormula(self, element):
        k = element.k
        if k == 1:
            smaller_formula = element.left
        else:
            from src.utils.formula import ANextFormula
            smaller_formula = ANextFormula(k - 1, element.left)
        constrs_to_add = []
        full_transition_milps = []
        root_state_vars = self.state_vars
        for i in range(self.bf):
            # Add constraints for agent network.
            action_grb_vars, action_constrs = self.agent.get_constraints_for_action(self.constrs_manager, root_state_vars)
            constrs_to_add.extend(action_constrs)

            # Add and get constraints for transition function.
            output_state_vars, output_state_constrs = \
                self.env.get_constraints_for_transition(i, self.constrs_manager, action_grb_vars, root_state_vars)
            constrs_to_add.extend(output_state_constrs)

            current_transition_milp = [action_constrs + output_state_constrs]

            self.state_vars = output_state_vars

            smaller_formula_milps = smaller_formula.acceptI(self)
            full_transition_milp = list(l + r for l, r in itertools.product(current_transition_milp, smaller_formula_milps))
            full_transition_milps.append(full_transition_milp)
        result = list(functools.reduce(lambda x, y: x + y, l) for l in itertools.product(*full_transition_milps))
        return result
