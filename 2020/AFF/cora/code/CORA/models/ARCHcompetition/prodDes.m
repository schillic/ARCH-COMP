function f = prodDes(x,u)
% differential equation for the production-destruction system from the ARCH
% competition

    a = 0.3;
    
    f(1,1) = (-x(1)*x(2))/(1+x(1));
    f(2,1) = (x(1)*x(2))/(1+x(1)) - a * x(2);
    f(3,1) = a * x(2);

end