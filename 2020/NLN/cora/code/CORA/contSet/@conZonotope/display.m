function display(obj)
% display - Displays the center, generators and constraints of a conZonotope
%
% Syntax:  
%    display(obj)
%
% Inputs:
%    obj - conZonotope object
%
% Outputs:
%    ---
%
% Example: 
%    Z=conZonotope(rand(2,6));
%    display(Z);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Dmitry Grebenyuk
% Written:       20-December-2017
% Last update:   01-May-2020 (MW, added empty case)
% Last revision: ---

%------------- BEGIN CODE --------------

name = [inputname(1), ' = '];
disp(name)

if isempty(obj)
    
    dispEmptyObj(obj,inputname(1));

else
    
    fprintf(newline);
    disp(inputname(1) + " =");
    fprintf(newline);

    % display center and generators
    disp('c: ');
    disp(obj.Z(:,1));

    disp('G: ');
    disp(obj.Z(:,2:end));

    % display constraints
    disp('A: ');
    disp(obj.A);

    disp('b: ');
    disp(obj.b);

end


end
%------------- END OF CODE --------------