function display(Z)
% display - Displays the center and generators of a zonotope
%
% Syntax:  
%    display(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%    ---
%
% Example: 
%    Z=zonotope(rand(2,6));
%    display(Z);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      14-September-2006 
% Last update:  22-March-2007
%               27-Aug-2019
%               01-May-2020 (MW, add empty case)
% Last revision:---

%------------- BEGIN CODE --------------

if isempty(Z)
    
    dispEmptyObj(Z,inputname(1));
    
else
    
    %display id, dimension
    display(Z.contSet);

    %display center
    disp('c: ');
    disp(center(Z));

    %display generators
    disp('g_i: ');
    disp(generators(Z));
    
end

%------------- END OF CODE --------------