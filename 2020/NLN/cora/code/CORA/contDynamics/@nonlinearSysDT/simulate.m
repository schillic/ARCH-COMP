function [t,x,ind] = simulate(obj,params,varargin)
% simulate - simulates a nonlinear discrete-time system
%
% Syntax:  
%    [t,x,ind] = simulate(obj,params)
%    [t,x,ind] = simulate(obj,params,odeOpts)
%
% Inputs:
%    obj - nonlinearSysDT object
%    params - struct containing the parameters for the simulation
%       .tStart: initial time
%       .tFinal: final time
%       .timeStep: time step size
%       .x0: initial point
%       .u: piecewise constant input signal u(t) specified as a matrix
%           for which the number of rows is identical to the number of
%           system input
%    options - ODE45 options (for hybrid systems)
%
% Outputs:
%    t - time vector
%    x - state vector
%    index - returns the event which has been detected
%
% Example: 
%    options.timeStep = 0.015;
%    options.u = [0.1 0.05 0.05 -0.1; 2 -1 0 -2];
%    options.tensorOrder = 2;
%
%    sys = nonlinearSysDT(2,2,@cstrDiscr,options);
%
%    [t,x] = simulate(sys,options,0,0.06,[-0.15;-45]);
%
%    plot(x(:,1),x(:,2),'.k','MarkerSize',20);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: linearSysDT/simulate

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      22-August-2012
% Last update:  29-January-2018 (NK)
%               24-March-2020 (NK)
%               08-May-2020 (MW, update interface)
% Last revision:---

%------------- BEGIN CODE --------------

% set initial state
t = (params.tStart:params.timeStep:params.tFinal);
x = zeros(length(t),length(params.x0));

x(1,:) = params.x0';

% consider changing inputs
if size(params.u,2) ~= 1
    change = 1;
else
    change = 0; 
end

% loop over all time steps
for i = 1:length(t)-1

    if change
        temp = obj.mFile(x(i,:)',params.u(:,i),params.timeStep);
    else
        temp = obj.mFile(x(i,:)',params.u,params.timeStep);
    end

    x(i+1,:) = temp';
end

ind = [];
    
%------------- END OF CODE --------------