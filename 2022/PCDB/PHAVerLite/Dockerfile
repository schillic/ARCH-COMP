# FROM ubuntu:latest
FROM ubuntu:20.04

MAINTAINER enea.zaffanella@unipr.it

RUN apt-get -y update
RUN apt-get -y install apt-utils software-properties-common wget
RUN apt-get -y install g++ make autoconf automake libtool
RUN apt-get -y install bison flex libgmp-dev libmpfr-dev libflint-dev

RUN mkdir /usr/PHAVerLite
WORKDIR /usr/PHAVerLite

# Download PPLite 0.7.1 sources
RUN wget https://github.com/ezaffanella/PPLite/raw/main/releases/pplite-0.7.1.tar.gz
RUN tar zxf pplite-0.7.1.tar.gz

# Download PHAVerLite 0.4 sources
RUN wget https://github.com/ezaffanella/PHAVerLite/raw/main/releases/phaverlite-0.4.tar.gz
RUN tar zxf phaverlite-0.4.tar.gz

# Download arch-comp22 models and config files
RUN wget https://github.com/ezaffanella/PHAVerLite/raw/main/ARCH-COMP/arch-comp22.tar.gz
RUN tar zxf arch-comp22.tar.gz

# Build and install PPLite 0.7.1
RUN mkdir -p build/pplite
WORKDIR /usr/PHAVerLite/build/pplite
RUN ../../pplite-0.7.1/configure --enable-optimization=sspeed
RUN make -j2 && make install
WORKDIR /usr/PHAVerLite

# Build and install PHAVerLite 0.4
RUN mkdir -p build/phaverlite
WORKDIR /usr/PHAVerLite/build/phaverlite
RUN ../../phaverlite-0.4/configure --disable-assertions --disable-debugging --enable-optimization=sspeed --with-cxxflags='-s'
RUN make -j2
WORKDIR /usr/PHAVerLite

# Create symbolic link to stand-alone executable
RUN ln -s build/phaverlite/src/phaverlite_static phaverlite-0.4_static

# Run all tests and produce results.csv file
RUN ./measure_all

# Show result file
RUN cat results.csv
