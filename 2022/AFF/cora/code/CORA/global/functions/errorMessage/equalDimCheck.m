function equalDimCheck(obj,S)
% equalDimCheck - checks if two objects (second argument can be class
%   array or numeric matrix) have the same dimension
%
% Syntax:  
%    equalDimCheck(obj,S)
%
% Inputs:
%    obj - contSet object
%    S   - contSet array or numeric matrix
%
%
% Example:
%    obj1 = ellipsoid.generateRandom('Dimension',2);
%    obj2 = zonotope.generateRandom('Dimension',2);
%    obj3 = zonotope.generateRandom('Dimension',3);
%    equalDimCheck(obj1,[obj2,obj3]); % throws error
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Victor Gassmann
% Written:      05-July-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
% check if dimensions match
if isnumeric(S)
    if dim(obj)~=size(S,1)
        throw(CORAerror('CORA:dimensionMismatch','obj1',obj,...
                            'dim1',dim(obj),'obj2',S,'size2',size(S)));
    end
else
    ind_dim = dim(obj)==dim(S);
    if ~all(ind_dim)
        S_i = S(find(~ind_dim,1));
        throw(CORAerror('CORA:dimensionMismatch','obj1',obj,...
                                'dim1',dim(obj),'obj2',S_i,'dim2',dim(S_i)));
    end
end
%------------- END OF CODE --------------
