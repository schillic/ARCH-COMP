function [Hf,Hg]=hessianTensorInt_bus3Dyn(x,y,u)

% helper variables to reduce number of evaluations
cosy4 = cos(y(4));
siny4 = sin(y(4));
y1siny4 = y(1) * siny4;
y1cosy4 = y(1) * cosy4;
cosy4my6 = cos(y(4) - y(6));
siny4my6 = sin(y(4) - y(6));
y3siny4my6 = y(3) * siny4my6;
y3cosy4my6 = y(3) * cosy4my6;
cosy5my6 = cos(y(5) - y(6));
y2cosy5my6 = y(2) * cosy5my6;
y3cosy5my6 = y(3) * cosy5my6;
y2y3cosy5my6 = y(2) * y3cosy5my6;
siny5my6 = sin(y(5) - y(6));
y2siny5my6 = y(2) * siny5my6;
y3siny5my6 = y(3) * siny5my6;
y2y3siny5my6 = y(2) * y3siny5my6;

 Hf{1} = interval(sparse(10,10),sparse(10,10));

Hf{1}(6,3) = (144115188075855872*cosy4)/611643854850417;
Hf{1}(3,6) = (144115188075855872*cosy4)/611643854850417;
Hf{1}(6,6) = -(144115188075855872*y1siny4)/611643854850417;


 Hf{2} = interval(sparse(10,10),sparse(10,10));



 Hg{1} = interval(sparse(10,10),sparse(10,10));

Hg{1}(6,3) = -5*cosy4;
Hg{1}(6,5) = -10*cosy4my6;
Hg{1}(8,5) = 10*cosy4my6;
Hg{1}(3,6) = -5*cosy4;
Hg{1}(5,6) = -10*cosy4my6;
Hg{1}(6,6) = 5*y1siny4 + 10*y3siny4my6;
Hg{1}(8,6) = -10*y3siny4my6;
Hg{1}(5,8) = 10*cosy4my6;
Hg{1}(6,8) = -10*y3siny4my6;
Hg{1}(8,8) = 10*y3siny4my6;


 Hg{2} = interval(sparse(10,10),sparse(10,10));

Hg{2}(5,4) = -(20*siny5my6)/3;
Hg{2}(7,4) = -(20*y3cosy5my6)/3;
Hg{2}(8,4) = (20*y3cosy5my6)/3;
Hg{2}(4,5) = -(20*siny5my6)/3;
Hg{2}(7,5) = -(20*y2cosy5my6)/3;
Hg{2}(8,5) = (20*y2cosy5my6)/3;
Hg{2}(4,7) = -(20*y3cosy5my6)/3;
Hg{2}(5,7) = -(20*y2cosy5my6)/3;
Hg{2}(7,7) = (20*y2y3siny5my6)/3;
Hg{2}(8,7) = -(20*y2y3siny5my6)/3;
Hg{2}(4,8) = (20*y3cosy5my6)/3;
Hg{2}(5,8) = (20*y2cosy5my6)/3;
Hg{2}(7,8) = -(20*y2y3siny5my6)/3;
Hg{2}(8,8) = (20*y2y3siny5my6)/3;


 Hg{3} = interval(sparse(10,10),sparse(10,10));

Hg{3}(5,4) = (20*siny5my6)/3;
Hg{3}(7,4) = (20*y3cosy5my6)/3;
Hg{3}(8,4) = -(20*y3cosy5my6)/3;
Hg{3}(4,5) = (20*siny5my6)/3;
Hg{3}(6,5) = 10*cosy4my6;
Hg{3}(7,5) = (20*y2cosy5my6)/3;
Hg{3}(8,5) = - 10*cosy4my6 - (20*y2cosy5my6)/3;
Hg{3}(5,6) = 10*cosy4my6;
Hg{3}(6,6) = -10*y3siny4my6;
Hg{3}(8,6) = 10*y3siny4my6;
Hg{3}(4,7) = (20*y3cosy5my6)/3;
Hg{3}(5,7) = (20*y2cosy5my6)/3;
Hg{3}(7,7) = -(20*y2y3siny5my6)/3;
Hg{3}(8,7) = (20*y2y3siny5my6)/3;
Hg{3}(4,8) = -(20*y3cosy5my6)/3;
Hg{3}(5,8) = - 10*cosy4my6 - (20*y2cosy5my6)/3;
Hg{3}(6,8) = 10*y3siny4my6;
Hg{3}(7,8) = (20*y2y3siny5my6)/3;
Hg{3}(8,8) = - 10*y3siny4my6 - (20*y2y3siny5my6)/3;


 Hg{4} = interval(sparse(10,10),sparse(10,10));

Hg{4}(6,3) = -5*siny4;
Hg{4}(6,5) = -10*siny4my6;
Hg{4}(8,5) = 10*siny4my6;
Hg{4}(3,6) = -5*siny4;
Hg{4}(5,6) = -10*siny4my6;
Hg{4}(6,6) = - 5*y1cosy4 - 10*y3cosy4my6;
Hg{4}(8,6) = 10*y3cosy4my6;
Hg{4}(5,8) = 10*siny4my6;
Hg{4}(6,8) = 10*y3cosy4my6;
Hg{4}(8,8) = -10*y3cosy4my6;


 Hg{5} = interval(sparse(10,10),sparse(10,10));

Hg{5}(4,4) = -40/3;
Hg{5}(5,4) = (20*cosy5my6)/3;
Hg{5}(7,4) = -(20*y3siny5my6)/3;
Hg{5}(8,4) = (20*y3siny5my6)/3;
Hg{5}(4,5) = (20*cosy5my6)/3;
Hg{5}(7,5) = -(20*y2siny5my6)/3;
Hg{5}(8,5) = (20*y2siny5my6)/3;
Hg{5}(4,7) = -(20*y3siny5my6)/3;
Hg{5}(5,7) = -(20*y2siny5my6)/3;
Hg{5}(7,7) = -(20*y2y3cosy5my6)/3;
Hg{5}(8,7) = (20*y2y3cosy5my6)/3;
Hg{5}(4,8) = (20*y3siny5my6)/3;
Hg{5}(5,8) = (20*y2siny5my6)/3;
Hg{5}(7,8) = (20*y2y3cosy5my6)/3;
Hg{5}(8,8) = -(20*y2y3cosy5my6)/3;


 Hg{6} = interval(sparse(10,10),sparse(10,10));

Hg{6}(5,4) = (20*cosy5my6)/3;
Hg{6}(7,4) = -(20*y3siny5my6)/3;
Hg{6}(8,4) = (20*y3siny5my6)/3;
Hg{6}(4,5) = (20*cosy5my6)/3;
Hg{6}(5,5) = -100/3;
Hg{6}(6,5) = -10*siny4my6;
Hg{6}(7,5) = -(20*y2siny5my6)/3;
Hg{6}(8,5) = 10*siny4my6 + (20*y2siny5my6)/3;
Hg{6}(5,6) = -10*siny4my6;
Hg{6}(6,6) = -10*y3cosy4my6;
Hg{6}(8,6) = 10*y3cosy4my6;
Hg{6}(4,7) = -(20*y3siny5my6)/3;
Hg{6}(5,7) = -(20*y2siny5my6)/3;
Hg{6}(7,7) = -(20*y2y3cosy5my6)/3;
Hg{6}(8,7) = (20*y2y3cosy5my6)/3;
Hg{6}(4,8) = (20*y3siny5my6)/3;
Hg{6}(5,8) = 10*siny4my6 + (20*y2siny5my6)/3;
Hg{6}(6,8) = 10*y3cosy4my6;
Hg{6}(7,8) = (20*y2y3cosy5my6)/3;
Hg{6}(8,8) = - 10*y3cosy4my6 - (20*y2y3cosy5my6)/3;
