function completed = example_neuralNet_reach_03_TORA
% example_neuralNet_reach_03_TORA - example of reachability analysis for a
%                                   neural network controlled cart
%
%
% Syntax:
%    completed = example_neuralNet_reach_03_TORA()
%
% Inputs:
%    no
%
% Outputs:
%    completed - boolean
%
% Reference:
%   [1] Johnson, Taylor T., et al. "ARCH-COMP21 Category Report:
%       Artificial Intelligence and Neural Network Control Systems (AINNCS)
%       for Continuous and Hybrid Systems Plants."
%       EPiC Series in Computing 80 (2021): 90-119.
%   [2] Dutta, Souradeep, et al. "Reachability analysis for
%       neural feedback systems using regressive polynomial rule inference"
%       Proceedings of the 22nd ACM International Conference on
%       Hybrid Systems: Computation and Control. 2019.
%   [3] Jankovic, Mrdjan, et al. "TORA example:
%       cascade-and passivity-based control designs."
%       IEEE Transactions on Control Systems Technology 4.3 (1996): 292-297
%
% Author:       Niklas Kochdumper, Tobias Ladner
% Written:      08-November-2021
% Last update:  20-May-2022 (TL: ARCH'22 Revisions)
% Last revision:---

%------------- BEGIN CODE --------------

disp("BENCHMARK: Sherlock-Benchmark-9 (TORA)")

% Parameter ---------------------------------------------------------------

R0 = interval([0.6;-0.7;-0.4;0.5],[0.7;-0.6;-0.3;0.6]);

params.tFinal = 20;
params.R0 = polyZonotope(R0);

% Reachability Settings ---------------------------------------------------

options.timeStep = 0.05;
options.taylorTerms = 4;
options.zonotopeOrder = 200;
options.alg = 'lin';
options.tensorOrder = 3;
options.errorOrder = 10;
options.intermediateOrder = 50;

% Parameters for NN evaluation --------------------------------------------
evParams = struct();
evParams.bound_approx = true;
evParams.polynomial_approx = "lin";
evParams.add_approx_error_to_Grest = true;
evParams.remove_Grest = false;

% System Dynamics ---------------------------------------------------------

% open-loop system
f = @(x,u) [x(2); -x(1) + 0.1*sin(x(3)); x(4); u(1) - 10];
sys = nonlinearSys(f);

% load neural network controller
% [4, 100, 100, 100, 1]
load('controllerTORA.mat');
nn = neuralNetwork(W, b, 'ReLU');

% construct neural network controlled system
sys = neurNetContrSys(sys, nn, 1);

% Specification -----------------------------------------------------------

safeSet = 2 * interval(-ones(4, 1), ones(4, 1));
spec = specification(safeSet, 'safeSet');

% Simulation --------------------------------------------------------------

tic
simRes = simulateRandom(sys, params);
tSim = toc;
disp(['Time to compute random Simulations: ', num2str(tSim)]);

% Check Violation --------------------------------------------------------

tic
isVio = false;
for i = 1:length(simRes.x)
    x = simRes.x{i};
    for j=1:length(safeSet)
        isVio = isVio || ~all( ...
            (infimum(safeSet(j)) <= x(:, j)) & ...
            (x(:, j) <= supremum(safeSet(j))));
    end
end
tVio = toc;
disp(['Time to check Violation in Simulations: ', num2str(tVio)]);

if isVio
    disp("Result: VIOLATED")
    R = params.R0;
    tComp = 0;
    tVeri = 0;
else
    % Reachability Analysis -----------------------------------------------

    tic
    R = reach(sys, params, options, evParams);
    tComp = toc;
    disp(['Time to compute Reachable Set: ', num2str(tComp)]);

    % Verification --------------------------------------------------------

    tic
    isVeri = true;
    for i = 1:length(R)
        R_i = R(i);
        for j = 1:length(R_i.timeInterval)
            isVeri = isVeri & safeSet.in(R_i.timeInterval.set{j});
        end
    end
    tVeri = toc;
    disp(['Time to check Verification: ', num2str(tVeri)]);

    if isVeri
        disp('Result: VERIFIED');
    else
        disp('Result: UNKOWN')
    end
end

disp(['Total Time: ', num2str(tSim+tVio+tComp+tVeri)]);

% Visualization -----------------------------------------------------------
disp("Plotting..")

figure;
hold on;
box on;
ss = plot(spec.set, [1, 2], 'FaceColor', [0, .8, 0]);
rs = plot(R, [1, 2], 'FaceColor', [.7, .7, .7]);
is = plot(R0, [1, 2], 'FaceColor', 'w', 'EdgeColor', 'k');
sims = plot(simRes, [1, 2], 'k');
xlabel('$x_1$ (distance)', 'interpreter', 'latex');
ylabel('$x_2\ (\dot{x_1})$', 'interpreter', 'latex')
xlim([-2.5, 2.5]);
ylim([-2.5, 2.5]);
legend([ss, rs, is, sims], "Safe Set", "Reachable Set", "Initial Set", "Simulations")

figure;
hold on;
box on;
ss = plot(spec.set, [3, 4], 'FaceColor', [0, .8, 0]);
rs = plot(R, [3, 4], 'FaceColor', [.7, .7, .7]);
is = plot(R0, [3, 4], 'FaceColor', 'w', 'EdgeColor', 'k');
sims = plot(simRes, [3, 4], 'k');
xlabel('$x_3$ (angle)', 'interpreter', 'latex');
ylabel('$x_4\ (\dot{x_3})$', 'interpreter', 'latex')
xlim([-2.5, 2.5]);
ylim([-2.5, 2.5]);
legend([ss, rs, is, sims], "Safe Set", "Reachable Set", "Initial Set", "Simulations")

% example completed
completed = 1;

%------------- END OF CODE --------------