function obj = getRandom(neurons_in, neurons_out, num_layers, activation)
% getRandom - creates a random layer-based network
%
% Syntax:
%    res = NeuralNetwork.getRandom(neurons_in, neurons_out, num_layers, activation)
%
% Inputs:
%    neurons_in: number of input neurons; default randi([1, 5])
%    neurons_out: number of output neurons; default randi([1, 5])
%    num_layers: number of layers; default randi([1, 5])
%    activation: list of activations to choose randomly from ["ReLU", "tanh", "sigmoid"]
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork

% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% validate parameters
if nargin < 1 || isempty(neurons_in)
    neurons_in = randi([1, 5]);
end
if nargin < 2 || isempty(neurons_out)
    neurons_out = randi([1, 5]);
end
if nargin < 3 || isempty(num_layers)
    num_layers = randi([1, 5]);
end
if nargin < 4 || isempty(activation)
    activation = ["ReLU", "tanh", "sigmoid"];
elseif ~isa(activation, "cell")
    activation = [activation];
end

% determine neurons in each layer
neurons = zeros(1, 1+num_layers);
neurons(1) = neurons_in;
for i = 1:num_layers - 1
    neurons(1+i) = randi([1, 20]);
end
neurons(end) = neurons_out;

% create layers
layers = cell(2*(length(neurons) - 1), 1);

scale = 1;
for i = 1:length(neurons) - 1
    % add linear layer
    W = rand(neurons(i+1), neurons(i)) * scale - scale / 2;
    b = rand(neurons(i+1), 1) * scale - scale / 2;
    layers{2*i-1} = NNLinearLayer(W, b);

    % add activation layers
    activation_i = activation(randi([1, length(activation)]));
    layer = NNActivationLayer.instantiateFromString(activation_i);
    layers{2*i} = layer;
end

% create NeuralNetwork
obj = NeuralNetwork(layers);
end