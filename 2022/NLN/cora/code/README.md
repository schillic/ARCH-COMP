## Results for CORA

The MATLAB script [mainCORA.m](/code/mainCORA.m) reproduces the results for the reachability toolbox CORA. In particular, the script computes the occupancy sets for all traffic scenarios and saves them in the directory **/results/solutions**. 

## Collision Checker

The python file [check_collision.py](/code/check_collision.py) runs the CommonRoad drivability checker to check if the exported occupancy sets collide with static or dynamic obstacles from the traffic scenarios. Details on how to install the CommonRoad drivability checker can be found in the file [postInstall](/environment/postInstall).

## Adding new Traffic Scenarios

To add a new traffic scenario one has to add the CommonRoad .xml-file with the traffic scneario description (see [https://commonroad.in.tum.de](https://commonroad.in.tum.de)) to the directory **/data/scenarios**. In addition one has to add the .xml-file with the CommonRoad solution trajectory to the directory **/data/trajectory**. Afterward, one then has to run the MATLAB script [addTrafficScenarios.m](/code/addTrafficScenarios.m), which generates the feedback matrices for tracking the reference trajectory and saves them together with the initial state and the control inputs for the reference trajectory to a .csv-file in the directory **/data/inputs**.