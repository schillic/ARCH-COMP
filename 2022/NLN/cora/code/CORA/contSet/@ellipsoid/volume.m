function val = volume(E)
% volume - Computes the volume of an ellipsoid acc. to Sec. 2 in [1]
%
% Syntax:  
%    val = volume(E)
%
% Inputs:
%    E - ellipsoid object/array
%
% Outputs:
%    val - volume
%
% Example: 
%    E = ellipsoid([1,0;0,3],[1;-1]);
%    val = volume(E);
%
% References:
%    [1] A. Moshtagh. "Minimum volume enclosing ellipsoid", 2005
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      28-August-2019
% Last update:  04-July-2022 (VG: allow class array input)
% Last revision:---

%------------- BEGIN CODE --------------
val = zeros(size(E));
ind = isempty(E(:));
val(ind) = 0;
tmp = 1:numel(E);
ii_rem = tmp(~ind);
for i=ii_rem
    n = length(E(i).Q);
    % use volume for n-ball
    Vball = pi^(n/2)/gamma(n/2+1);
    val(i) = Vball*sqrt(det(E(i).Q));
end
%------------- END OF CODE --------------