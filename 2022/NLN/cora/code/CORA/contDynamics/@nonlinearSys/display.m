function display(sys)
% display - Displays a nonlinearSys object on the command window
%
% Syntax:  
%    display(sys)
%
% Inputs:
%    sys - nonlinearSys object
%
% Outputs:
%    ---
%
% Example:
%    f = @(x,u) [x(2); ...
%               (1-x(1)^2)*x(2)-x(1)];
%    sys = nonlinearSys('vanDerPol',f)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff, Mark Wetzlinger
% Written:      17-October-2007
% Last update:  19-June-2022
% Last revision:---

%------------- BEGIN CODE --------------

fprintf(newline);

disp([inputname(1), ' =']);

fprintf(newline);

%display parent object
display@contDynamics(sys);

%display type
disp('Type: Nonlinear continuous-time system');

%create symbolic variables
vars = symVariables(sys);

%insert symbolic variables into the system equations
f = sys.mFile(vars.x,vars.u);

%display state space equations
disp('State-space equations:')
for i=1:length(f)
    disp(['  f(',num2str(i),') = ',char(f(i))]);
end

fprintf(newline);

%------------- END OF CODE --------------